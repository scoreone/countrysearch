

var countriesData = {};
var sortCountriesAlphabetically = true;

function isValidForm() {
	let countryField = document.getElementById('country');
    let searchVal = document.getElementById('country').value;
    let msg = '';
    if (searchVal.trim().length < 1) {
		msg = 'Country name or country code must not be blank.';
	    displaySearchError(msg);
		countryField.setCustomValidity("Please provide a search term.");
	    return false;
    }
    hideSearchError();
    return true;
}

function displaySearchError(msg='Error') {
	let errorElem = document.getElementById('searchError');
	errorElem.innerHTML = msg;
	errorElem.style.display = 'block';
}

function hideSearchError() {
	let errorElem = document.getElementById('searchError');
	errorElem.innerHTML = '';
	errorElem.style.display = 'none';
}  	

function checkSearchTerm() {
	let countryField = document.getElementById('country');
	let constraint = new RegExp('^[A-Za-z ]+$');
	if (constraint.test(countryField.value)) {
		countryField.setCustomValidity("");
	} else {
		countryField.setCustomValidity("Only characters and spaces are valid.");
	}	
}

function toggleSortType() {
	sortCountriesAlphabetically = !sortCountriesAlphabetically;
	displayCountries();
}

function languagesString(langArr) {
	let langs = [];
	for (let lang of langArr) {
		langs.push(lang.name);
	}
	return langs.join(", ");
}

function createCountryElem(countryInfo) {
	let countryElem = "<div>[img][name][table]</div>";
	let img = "<img src='" + countryInfo.flag + "'>";
	let name = "<span class='countryName'>" + countryInfo.name + "</span>";
	let codes = "(" + countryInfo.alpha2Code + ", " + countryInfo.alpha3Code + ")";
	let codeRow = "<tr><td>Codes:</td><td>" + codes + "</td>";
	let regionRow = "<tr><td>Region:</td><td>" + countryInfo.region + "</td>";
	let subregionRow = "<tr><td>Subregion:</td><td>" + countryInfo.subregion + "</td>";
	let popRow = "<tr><td>Population:</td><td>" + countryInfo.population + "</td>";
	let langRow = "<tr><td>Languages:</td><td>" + languagesString(countryInfo.languages) + "</td>";
	let table = "<table>" + codeRow + regionRow + subregionRow + popRow + langRow + "</table>";
		
	countryElem = countryElem.replace("[img]", img);
	countryElem = countryElem.replace("[name]", name);
	countryElem = countryElem.replace("[table]", table);
	
	return countryElem;	
}

function displayCounts() {
	let count = countriesData.count;
	let regions = countriesData.regions;
	let subregions = countriesData.subregions;
	let countInfo = "Countries returned: " + count.toString();
	let regionCounts = [];
	let subregionCounts = [];
	
	for (let region in regions) {
		regionCounts.push(region + " (" + regions[region] + ")");
	}

    for (let subregion in subregions) {
		subregionCounts.push(subregion + " (" + subregions[subregion] + ")");
	}		
	
	countInfo += "<br>Regions: ";
	countInfo += regionCounts.join(", ");
    countInfo += "<br>Subregions: ";
	countInfo += subregionCounts.join(", ");	
	
	$("#counts").html(countInfo);	
}

function displayCountries() {
	let display = "";
	let countries = sortCountriesAlphabetically ? countriesData.sortedAlpha : countriesData.sortedPop;
	let sortTypeHeader = sortCountriesAlphabetically ? "Countries sorted alphabetically" : "Countries sorted by population";
	let sortTypeButton = sortCountriesAlphabetically ? "Sort by population" : "Sort alphabetically";

	if (countriesData.count > 1) {
	    $('#sortType').html(sortTypeHeader);
	    $('#sortButton').text(sortTypeButton);
		$('#sortTypeDiv').show();
	} else {
		$('#sortTypeDiv').hide();
	}
	
	for (let country of countries) {
		display += createCountryElem(country);
	}
	
	$('#results').html(display);
	
	// alternate colors
	$('#results div:even').css('background-color', '#d7dbdd');
	$('#results div:odd').css('background-color', '#f2f3f4');
}

$(document).ready(function(){
	var form = $('#formCountries');
	var resultsDiv = $('#results');
	var ajaxFailMessage = "Sorry, a problem occurred while fetching the results. ";
	document.getElementById('country').oninput = checkSearchTerm;
	$('#sortTypeDiv').hide();
	
	$(form).submit(function(event) {
		event.preventDefault();
		
		if (isValidForm()) {
		    let searchVal = $('#country').val();
		    let formData = $(form).serialize();
			let maxResults = 50;
		     
		    $.ajax({
				dataType: 'json', 
			    type: 'POST',
			    url: $(form).attr('action'),
			    data: formData
		    }).done(function(response) {
				
				countriesData = response;
				
				let count = countriesData.count;
				if (count < 1) {
					displaySearchError('Sorry, your query returned zero results.');
				} else if (count >= maxResults) {
					displaySearchError('The maximum of ' + maxResults.toString() + ' results was reached.');
				}
				
				displayCountries();
				displayCounts();
				
			}).fail(function(data) {
				let msg = ajaxFailMessage + data.responseText;
				displaySearchError(msg);
			});
		    
		}
	});
	
});

