
<?php
/*
Lookup country info using the REST Countries API:
https://restcountries.eu/

Lookup by name (includes partial matches)
https://restcountries.eu/rest/v2/name/{name}
https://restcountries.eu/rest/v2/name/united

Lookup by country code (2 or 3 char code)
https://restcountries.eu/rest/v2/alpha/{code}
https://restcountries.eu/rest/v2/alpha/col

*/

// Filter the fields that are returned
$fields = array('name', 'alpha2Code', 'alpha3Code', 'flag', 'region', 'subregion', 'population', 'languages');
$fieldsText = implode(';', $fields);
$dataFilter = "?fields=" . $fieldsText;
define("DATA_FILTER", $dataFilter);

// URL for name lookup
function nameUrl($name, $filter = DATA_FILTER) {
	$url = "https://restcountries.eu/rest/v2/name/" . $name . $filter;
	return $url;
}

// URL for code lookup
function alphaUrl($code, $filter = DATA_FILTER) {
	$url = "https://restcountries.eu/rest/v2/alpha/" . $code . $filter;
	return $url;
}

// JSON text
function getDataFromUrl($url) {
  try {  
    $data = @file_get_contents($url);		
  } catch (Exception $e) {
	$data = "";
  }
  return $data;  
}

// Comparison function used to sort results by country name (ascending)
function compareName($a, $b) {
	return strcasecmp($a['name'], $b['name']);		
}

// Comparison function used to sort results by country population (descending)
function comparePopulation($a, $b) {
	return $b['population'] <=> $a['population'];
}

function hasAllFields($arr, $fields) {
	$hasAll = true;
	if (!is_array($arr)) {
		return false;
	}
	if (!is_array($fields)) {
		if (!array_key_exists($fields, $arr)) {
			return false;
		}
	}
	foreach ($fields as $field) {
		if (!array_key_exists($field, $arr)) {
			$hasAll = false;
			break;
		}
	}
	return $hasAll;
}

// Handle 404 status
// Handle NULL object
// Handle single object returned
// Make sure all fields were returned
function cleanResultArray($jsonResult) {
	global $fields;

	if (!is_array($jsonResult)) {
		if (($jsonResult && property_exists($jsonResult, 'status')) || !$jsonResult) {
			$jsonResult = array();
		} else {
			$jsonResult = array($jsonResult);
		}
	} 
	
	if (!empty($jsonResult)) {
		if (!array_key_exists(0, $jsonResult)) {
			$jsonResult = array($jsonResult);
		}			
		if (!hasAllFields($jsonResult[0], $fields)) {
			$jsonResult = array();
		}
	}
	
	return $jsonResult;
}

// Get country data given search term (could be country name or code)
function getResults($searchTerm, $maxResults = 50) {
	$charCount = mb_strlen($searchTerm, 'UTF-8');
	
	if ($charCount < 1) {
		return array();
	}	
	
	// Search by country name
	$nameResults = json_decode(getDataFromUrl(nameUrl($searchTerm)), true);
	
	// Search by country code
	if ($charCount == 2 || $charCount == 3) {
		$codeResults = json_decode(getDataFromUrl(alphaUrl($searchTerm)), true);
	} else {
		$codeResults = array();
	}
	
	// Clean results by handling edge cases
	$nameResults = cleanResultArray($nameResults);
	$codeResults = cleanResultArray($codeResults);
	
	// Combine results (name and code)
	$combined = array_merge($nameResults, $codeResults);
	
	// Limit results
	if (count($combined) > $maxResults) {
		$combined = array_slice($combined, 0, $maxResults);
	}	
	
	return $combined;
}

function sortedAlphabetically($countries) {
	$sorted = $countries;
	usort($sorted, "compareName");
	return $sorted;
}

function sortedPopulation($countries) {
	$sorted = $countries;
	usort($sorted, "comparePopulation");
	return $sorted;
}

// Find value counts for particular key
function valueCounts($arr, $keyVal) {
	$vals = array();
	foreach ($arr as $item) {
		if (array_key_exists($keyVal, $item)) {
			$value = $item[$keyVal];
			if (!$value) {
				$value = "None";
			}
			$vals[] = $value;
		}
	}
	sort($vals);
	$counts = array_count_values($vals);
	return $counts;	
}

// Add info used by the view
function addInfo($countries) {
	$info = array();
	$count = count($countries);
	$byAlpha = sortedAlphabetically($countries);
	$byPop = sortedPopulation($countries);
	$info['count'] = $count;
	$info['sortedAlpha'] = $byAlpha;
	$info['sortedPop'] = $byPop;
	$info['regions'] = valueCounts($countries, 'region');
	$info['subregions'] = valueCounts($countries, 'subregion');
	return $info;
}




$name = rawurlencode(htmlentities(trim($_POST["country"])));

$json = json_encode(addInfo(array_unique(getResults($name), SORT_REGULAR)));

echo $json;





function myprint($stuff){
	foreach($stuff as $item) {
		var_dump($item);
		echo "<br><br>";
	}
}

?>
