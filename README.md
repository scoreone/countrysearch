# README #

> command line:   php -S localhost:8080
>
> chrome browser: localhost:8080/lookup.html


### What is this repository for? ###

> This app allows the user to search countries(by country name or country code) to retrieve info about country's region, flag, population, and languages.
>
* Version 1

### How do I get set up? ###

> Use PHP 7's built-in server to run app:     php -S localhost:8080
>
> Once server is up and running, point latest Chrome browser to following URL:  localhost:8080/lookup.html

### Who do I talk to? ###
> Matthew Beard
> email: biozealot21@gmail.com
> [Country Search implementation] (http://mattbeard.tk/developer/countryData/lookup.html)
